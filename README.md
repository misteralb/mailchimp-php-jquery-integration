# MailChimp PHP/jQuery Integration #

Simple, basic and fully-functional mailchimp integration based on latest MailChimp SDK using PHP & jQuery programming languages.

## Installation ##

1. Open file: [ROOT]/inc/mcsdk.php
2. Enter your MailChimp API Key and your List ID into the specified constants.
3. You finished the configurations, now you are ready to go ...